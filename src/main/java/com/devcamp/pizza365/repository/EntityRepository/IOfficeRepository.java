package com.devcamp.pizza365.repository.EntityRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza365.entity.Office;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IOfficeRepository extends JpaRepository<Office, Integer> {
    // yêu cầu 2 Viết query cho bảng offices cho phép tìm danh sách theo họ hoặc city , address_line với LIKE
	@Query(value = "SELECT * FROM offices WHERE city LIKE :city%", nativeQuery = true)
	List<Office> findByCityLike(@Param("city") String city);

	@Query(value = "SELECT * FROM offices WHERE address_line LIKE :address_line%", nativeQuery = true)
	List<Office> findByFirstNameLike(@Param("address_line") String address_line);
	
	// yêu cầu 3 Viết query cho bảng offices cho phép tìm danh sách theo city, state với LIKE có phân trang.
	@Query(value = "SELECT * FROM offices WHERE city LIKE :city%", nativeQuery = true)  // yc 4 
	List<Office> findByCityLike(@Param("city") String city, Pageable pageable);  // domain
	
	@Query(value = "SELECT * FROM offices WHERE state LIKE :state%", nativeQuery = true)
	List<Office> findByStateLikePage(@Param("state") String state, Pageable pageable);

	// yêu cầu 4 Viết query cho bảng offices cho phép tìm danh sách theo city có phân trang và ORDER BY state.
	@Query(value = "SELECT * FROM offices WHERE city = :city ORDER BY state ASC", nativeQuery = true)
	List<Office> findByCountryLike(@Param("city") String country, Pageable pageable);

	// yc5  Viết query cho bảng offices cho phép UPDATE dữ liệu có state = NULL với giá trị truyền vào từ tham số
	@Transactional  // javax.transaction các cập nhật cơ sở dữ liệu sẽ được hủy bỏ nếu xảy ra lỗi trong quá trình thực hiện.
	@Modifying
	@Query(value = "UPDATE offices SET state = :state  WHERE state IS null", nativeQuery = true)
	int updateCountry(@Param("state") String state);

}
