package com.devcamp.pizza365.repository.EntityRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Product;
import java.util.List;

import javax.transaction.Transactional;


@Repository
public interface IproductsRepository extends JpaRepository<Product, Long> {
      Product  findById(int id);

      // yêu cầu 2 Viết query cho bảng products cho phép tìm danh sách theo product_name với LIKE
	@Query(value = "SELECT * FROM products WHERE product_name LIKE :%product_name%", nativeQuery = true)
	List<Product> findByProductNameLike(@Param("product_name") String productName);


	@Query(value = "SELECT * FROM products WHERE product_description LIKE :%product_description%", nativeQuery = true)  // yc 4 
	List<Product> findByProductDescriptionLike(@Param("product_description") String proDescription, Pageable pageable);

	// // yêu cầu 3 Viết query cho bảng products cho phép tìm danh sách theo product_vendor với LIKE có phân trang.
	@Query(value = "SELECT * FROM products WHERE product_vendor LIKE :product_vendor%", nativeQuery = true)
	List<Product> findByVendorLike(@Param("product_vendor") String product_vendor, Pageable pageable);

	// // yêu cầu 4 Viết query cho bảng products cho phép tìm danh sách theo product_line_id có phân trang và ORDER BY name.
	@Query(value = "SELECT * FROM products WHERE product_line_id = :product_line_id ORDER BY product_name ASC", nativeQuery = true)
	List<Product> findByProductLineIdLike(@Param("product_line_id") String product_line_id, Pageable pageable);

	// // yc5  Viết query cho bảng products cho phép UPDATE dữ liệu có country = NULL với giá trị truyền vào từ tham số
	@Transactional  //  javax.transaction các cập nhật cơ sở dữ liệu sẽ được hủy bỏ nếu xảy ra lỗi trong quá trình thực hiện.
	@Modifying
	@Query(value = "UPDATE products SET product_name = :product_name  WHERE product_name IS null", nativeQuery = true)
	int updateCountry(@Param("product_name") String productName);

    
}
