package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CVoucher;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    // SELECT * FROM `p_vouchers`
    // UPDATE `p_vouchers` SET `phan_tram_giam_gia` = '99' WHERE `p_vouchers`.`id` =
    // 1;
    // @Query(value = "SELECT * FROM customers WHERE last_name LIKE :lastName%",
    // nativeQuery = true)
    // List<Customer> findByLastNameLike(@Param("lastName") String lastName);
    @Query(value = "SELECT * FROM `p_vouchers`", nativeQuery = true)
    List<CVoucher> finAllVouchersThuong();

    @Transactional
    @Modifying

    @Query(value = "UPDATE p_vouchers SET phan_tram_giam_gia = :phantram WHERE ma_voucher  = :ma_voucher", nativeQuery = true)
    int updateVouchersSQL(@Param("ma_voucher") String maVoucher, @Param("phantram") String phantram);

    // @Query(value = "UPDATE p_vouchers SET phan_tram_giam_gia = :phanTram WHERE ma_voucher  = :ma_voucher", nativeQuery = true)
    // int updateVouchersSQL(@Param("ma_voucher") String maVoucher, @Param("phanTram") String phanTram);

}
