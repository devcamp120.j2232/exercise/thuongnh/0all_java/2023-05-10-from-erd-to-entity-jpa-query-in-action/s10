package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {
	// yêu cầu 2 Viết query cho bảng customers cho phép tìm danh sách theo họ hoặc tên với LIKE
	@Query(value = "SELECT * FROM customers WHERE last_name LIKE :lastName%", nativeQuery = true)
	List<Customer> findByLastNameLike(@Param("lastName") String lastName);

	@Query(value = "SELECT * FROM customers WHERE first_name LIKE :firstName%", nativeQuery = true)
	List<Customer> findByFirstNameLike(@Param("firstName") String firstName);
	
	// yêu cầu 3 Viết query cho bảng customers cho phép tìm danh sách theo city, state với LIKE có phân trang.
	@Query(value = "SELECT * FROM customers WHERE city LIKE :city%", nativeQuery = true)  // yc 4 
	List<Customer> findByCityLike(@Param("city") String city, Pageable pageable);  // domain
	
	@Query(value = "SELECT * FROM customers WHERE state LIKE :state%", nativeQuery = true)
	List<Customer> findByStateLike(@Param("state") String state, Pageable pageable);

	// yêu cầu 4 Viết query cho bảng customers cho phép tìm danh sách theo country có phân trang và ORDER BY tên.
	@Query(value = "SELECT * FROM customers WHERE country = :country ORDER BY first_name ASC", nativeQuery = true)
	List<Customer> findByCountryLike(@Param("country") String country, Pageable pageable);

	// yc5  Viết query cho bảng customers cho phép UPDATE dữ liệu có country = NULL với giá trị truyền vào từ tham số
	@Transactional  // javax.transaction các cập nhật cơ sở dữ liệu sẽ được hủy bỏ nếu xảy ra lỗi trong quá trình thực hiện.
	@Modifying
	@Query(value = "UPDATE customers SET country = :country  WHERE country IS null", nativeQuery = true)
	int updateCountry(@Param("country") String country);

	// yêu cầu 6 .   Làm tương tự cho payments , orders, order_details, products, product_lines, employees, offices 
}
