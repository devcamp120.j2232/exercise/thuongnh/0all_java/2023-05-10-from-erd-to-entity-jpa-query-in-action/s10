package com.devcamp.pizza365.controller.controllerEntity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.entity.Employee;

public interface CEmployeesController extends JpaRepository<Employee, Long>{
    
}
