package com.devcamp.pizza365.controller.controllerEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.repository.IUserRepository;
import com.devcamp.pizza365.repository.EntityRepository.IproductsRepository;

@CrossOrigin
@RestController
@RequestMapping("/products")
public class CProductsController {

    @Autowired
    private IproductsRepository pIproductsRepository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllUSer() {
        try {
            return new ResponseEntity<>(pIproductsRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/details/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable Long id) {
        try {
            Optional<Product> newProduct = pIproductsRepository.findById(id);
            return new ResponseEntity<>(newProduct, HttpStatus.OK);
            // return new ResponseEntity<>(pIproductsRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // JSON mẫu
    // "productCode": "S10_1678",
    // "productName": "1969 Harley Davidson Ultimate Chopper",
    // "productDescripttion": "This replica features working kickstand, front
    // suspension, gear-shift lever, footbrake lever, drive chain, wheels and
    // steering. All parts are particularly delicate due to their precise scale and
    // require special care and attention.",
    // "idProductLine": 2,
    // "productScale": "1:10",
    // "productVendor": "Min Lin Diecast",
    // "quantityInStock": 7933,
    // "buyPrice": 48.81

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody Product paramProduct) {
        try {
            Product newUser = new Product();
            newUser.setBuyPrice(paramProduct.getBuyPrice());
            newUser.setIdProductLine(paramProduct.getIdProductLine());
            newUser.setOrderDetails(paramProduct.getOrderDetails());
            newUser.setProductCode(paramProduct.getProductCode());
            newUser.setProductDescripttion(paramProduct.getProductDescripttion());
            newUser.setProductLine(paramProduct.getProductLine());
            newUser.setProductName(paramProduct.getProductName());
            newUser.setProductScale(paramProduct.getProductScale());
            newUser.setProductVendor(paramProduct.getProductVendor());
            newUser.setQuantityInStock(paramProduct.getQuantityInStock());

            Product savedUser = pIproductsRepository.save(newUser);
            return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
        } catch (Exception e) {
            // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified User: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") Long id,
            @RequestBody Product paramProduct) {
        Optional<Product> userData = pIproductsRepository.findById(id);
        if (userData.isPresent()) {
            Product newUser = userData.get();

            newUser.setBuyPrice(paramProduct.getBuyPrice());
            newUser.setIdProductLine(paramProduct.getIdProductLine());
            newUser.setOrderDetails(paramProduct.getOrderDetails());
            newUser.setProductCode(paramProduct.getProductCode());
            newUser.setProductDescripttion(paramProduct.getProductDescripttion());
            newUser.setProductLine(paramProduct.getProductLine());
            newUser.setProductName(paramProduct.getProductName());
            newUser.setProductScale(paramProduct.getProductScale());
            newUser.setProductVendor(paramProduct.getProductVendor());
            newUser.setQuantityInStock(paramProduct.getQuantityInStock());

            Product savedUser = pIproductsRepository.save(newUser);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified User: " + id);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
        try {
            pIproductsRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println("==================" + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
